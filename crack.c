#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
   char *guessHash = md5(guess, strlen(guess));
    //strcmp returning 0 means hashes matched
    if(strcmp(guessHash, hash) == 0){
        free(guessHash);
        //the hashes matched
        return 1;
    }
    else{
        free(guessHash);
        ///the hashes didn't match
        return 0;
    }
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    struct stat info;
    if ((stat(filename, &info)) == -1){
        printf("Can't stat the file\n");
        exit(1);
    }
    
    int filesize = info.st_size;
    char *contents = malloc(filesize + 1);
    
    FILE *in = fopen(filename, "r");
    if(!in){
        printf("Can't open file for reading");
        exit(1);
    }
    
    fread(contents, 1, filesize, in);
    fclose(in);
    contents[filesize] = '\0';
    
    int nlcount = 0;
    for(int i = 0; i < filesize; i++){
        if(contents[i] == '\n'){
            nlcount++;
        }
    }
    
    //printf("Line count is %d\n", nlcount);
    
    char **dictionary = malloc(nlcount * sizeof(char *));
    
    dictionary[0] = strtok(contents, "\n");
    int i = 1;
    while((dictionary[i] = strtok(NULL, "\n")) != NULL){
        i++;
    }
    
    //for(int j = 0; j < nlcount; j++){
    //    printf("%d %s \n", j, dictionary[j]);
    //}
    
    
    *size = nlcount;
    return dictionary;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    int hlen;
    char **dict = read_dictionary(argv[2], &dlen);
    char **hashes = read_dictionary(argv[1], &hlen);
    
    
    printf("Dictionary Length: %d\n", dlen);
    printf("Hash list Length: %d\n", hlen);
    
    int found = 1;
    for(int i = 0; i < hlen; i++){
        for(int j = 0; j < dlen; j++){
            //printf("Trying %s against %s\n", dict[j], hashes[i]);
            if(tryguess(hashes[i], dict[j]) == 1){
               printf("Hash#%d %s == %s\n", found, dict[j], hashes[i]);
               found++;
           }
        }
    }
    
    

    // Open the hash file for reading.
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
}
